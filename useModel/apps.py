from django.apps import AppConfig


class UsemodelConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'useModel'
