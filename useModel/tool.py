import time
from datetime import datetime

def bodyTypeFunc(x):
    resultStr = 0
    if x == '休旅車':
        resultStr = 5
    elif x == '重機':
        resultStr = 4
    elif x == '貨車':
        resultStr = 3
    elif x == '廂車':
        resultStr = 2
    elif x == '轎車':
        resultStr = 1
    else:
        pass
    return resultStr

def carEvuFunc(x):
    resultStr = 0
    if x == 'A++':
        resultStr = 9
    elif x == 'AAA':
        resultStr = 9
    elif x == 'A+':
        resultStr = 8
    elif x == 'AA':
        resultStr = 8
    elif x == 'A':
        resultStr = 7
    elif x == 'B++':
        resultStr = 6
    elif x == 'BBB':
        resultStr = 6
    elif x == 'B+':
        resultStr = 5
    elif x == 'BB':
        resultStr = 5
    elif x == 'B':
        resultStr = 4
    elif x == 'C++':
        resultStr = 4
    elif x == 'CCC':
        resultStr = 4
    elif x == 'C+':
        resultStr = 3
    elif x == 'CC':
        resultStr = 3
    elif x == 'C':
        resultStr = 2
    elif x == 'D':
        resultStr = 1
    elif x == 'N':
        resultStr = 0
    else:
        pass
    return resultStr

def insideEvuFunc(x):
    resultStr = 0
    if x == 'A':
        resultStr = 6
    elif x == 'B':
        resultStr = 5
    elif x == 'C':
        resultStr = 4
    elif x == 'D':
        resultStr = 3
    elif x == 'E':
        resultStr = 2
    elif x == 'N':
        resultStr = 1
    else:
        pass
    return resultStr

def ymMfgFunc(x, y):
    resultINT = 0
    try:
        if ('/' in str(x)):
            x = str(x).replace('/', '')[:6]
            ym = (datetime.timestamp(datetime.strptime(x, "%Y%m")))
        elif ('.' in str(x)):
            x = str(x).replace('.', '')[:6]
            ym = (datetime.timestamp(datetime.strptime(x, "%Y%m")))
        elif('-' in str(x)):
            x = str(x).replace('-', '')[:6]
            ym = (datetime.timestamp(datetime.strptime(x, "%Y%m")))
            pass
        elif(len(x) ==6):
            ym = (datetime.timestamp(datetime.strptime(x, "%Y%m")))
        else:
            ym = 0
    except Exception as e:
        ym = 0

    yy = datetime.timestamp(datetime.strptime(str(y)[:6], "%Y%m"))
    resultINT = (yy - ym)/60/60/24/365
    return resultINT

def milePromise(x):
    rtn = 1
    if x =='是':
        return rtn
    else:
        rtn = 0
        return rtn

def sellerType(x):
    rtn = 1
    if x == '經銷商委拍':
        return 2
    elif x == '租賃':
        return 3
    elif x == '其他租賃':
        return 3
    elif x == '一般委拍':
        return 2
    elif x == '個人委拍':
        return 2
    elif x == '收購':
        return 1
    elif x == '法拍':
        return 1
    elif x == '其他法拍':
        return 1
    else:
        return rtn


def colorFunx(x):
    resultInt = 0
    if x == '黑':
        return 6
    elif x == '白':
        return 5
    elif x == '銀':
        return 4
    elif x == '灰':
        return 3
    elif x == '藍':
        return 2
    elif x == '紅':
        return 1
    else:
        return resultInt