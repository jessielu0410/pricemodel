from curses.ascii import HT
from json import tool
from django.shortcuts import render
from django.http import HttpResponse
import lightgbm as lgb
import pandas as pd

from .tool import *

NewPrice = pd.read_csv('newPrice_2.csv')

# Create your views here.
def index(request):
    return render(request,"index.html")

# custom method for generating predictions
def getPredictions(df):
    try:
        prediction = 0
        # print(df['SOU'].values[0], type(df['SOU']))
        # if df['SOU'].values[0] == 3:
        #     print('租')
        #     model = lgb.Booster(model_file='lgbr_base_withNewPrice_租賃.txt')
        #     prediction = model.predict(df) #Use to predice
        # else:
        #     model = lgb.Booster(model_file='lgbr_base_withNewPrice_委拍.txt')
        #     prediction = model.predict(df) #Use to predice
        # fileName = ''
        # if df['EXHAUST'].values[0] < 1800:
        #     fileName = 'lgbr_base_withNewPrice_0331_1800.txt'
        # elif df['EXHAUST'].values[0] > 1800 and df['EXHAUST'].values[0] < 2400:
        #     fileName = 'lgbr_base_withNewPrice_0331_1801_2400.txt'
        # elif df['EXHAUST'].values[0] > 2400 and df['EXHAUST'].values[0] < 3000:
        #     fileName = 'lgbr_base_withNewPrice_0331_2401_3000.txt'     
        # else: 
        #     return '排氣量不符合計算範圍'
        fileName = 'lgbr_base_withNewPrice_0506.txt'
        model = lgb.Booster(model_file=fileName)
        prediction = model.predict(df) #Use to predice

        rtn = prediction**2 / 10000
        rtnStr = str(round(rtn[0],1)-3) + '萬 ~ ' +  str(round(rtn[0],1)+3) + '萬' #先輸出正負三萬
        return rtnStr
    except Exception as e:
        print(e)
        
# our result page view
def result(request):
    #All columns in our model
    column_names = ['EXHAUST', 'NUM_TRAVEL', 'OLD', 'INSIDEVU', 'CAREVU', 'SOU',
       'SalesMonth', 'COLOR', 'AveragePrice', 'BRAND_TYPE_FORD_ECONOVAN',
       'BRAND_TYPE_FORD_ECOSPORT', 'BRAND_TYPE_FORD_ESCAPE',
       'BRAND_TYPE_FORD_ESCORT', 'BRAND_TYPE_FORD_FIESTA',
       'BRAND_TYPE_FORD_FOCUS', 'BRAND_TYPE_FORD_KUGA',
       'BRAND_TYPE_FORD_MONDEO', 'BRAND_TYPE_FORD_RANGER',
       'BRAND_TYPE_FORD_TOURNEO', 'BRAND_TYPE_HONDA_ACCORD',
       'BRAND_TYPE_HONDA_CITY', 'BRAND_TYPE_HONDA_CIVIC',
       'BRAND_TYPE_HONDA_FIT', 'BRAND_TYPE_HONDA_INSIGHT',
       'BRAND_TYPE_HONDA_ODYSSEY', 'BRAND_TYPE_LUXGEN_M7',
       'BRAND_TYPE_LUXGEN_S3', 'BRAND_TYPE_LUXGEN_S5', 'BRAND_TYPE_LUXGEN_U5',
       'BRAND_TYPE_LUXGEN_U6', 'BRAND_TYPE_LUXGEN_U7', 'BRAND_TYPE_LUXGEN_URX',
       'BRAND_TYPE_LUXGEN_V7', 'BRAND_TYPE_MITSUBISHI_GRUNDER',
       'BRAND_TYPE_MITSUBISHI_LANCER', 'BRAND_TYPE_MITSUBISHI_OUTLANDER',
       'BRAND_TYPE_MITSUBISHI_SAVRIN', 'BRAND_TYPE_MITSUBISHI_ZINGER',
       'BRAND_TYPE_NISSAN_370Z', 'BRAND_TYPE_NISSAN_BLUEBIRD',
       'BRAND_TYPE_NISSAN_JUKE', 'BRAND_TYPE_NISSAN_KICKS',
       'BRAND_TYPE_NISSAN_LIVINA', 'BRAND_TYPE_NISSAN_MARCH',
       'BRAND_TYPE_NISSAN_MURANO', 'BRAND_TYPE_NISSAN_ROGUE',
       'BRAND_TYPE_NISSAN_SENTRA', 'BRAND_TYPE_NISSAN_SERENA',
       'BRAND_TYPE_NISSAN_TEANA', 'BRAND_TYPE_NISSAN_TIIDA',
       'BRAND_TYPE_TOYOTA_86', 'BRAND_TYPE_TOYOTA_ALPHARD',
       'BRAND_TYPE_TOYOTA_ALTIS', 'BRAND_TYPE_TOYOTA_AURIS',
       'BRAND_TYPE_TOYOTA_CAMRY', 'BRAND_TYPE_TOYOTA_INNOVA',
       'BRAND_TYPE_TOYOTA_PREVIA', 'BRAND_TYPE_TOYOTA_PRIUS',
       'BRAND_TYPE_TOYOTA_RAV4', 'BRAND_TYPE_TOYOTA_SIENNA',
       'BRAND_TYPE_TOYOTA_SIENTA', 'BRAND_TYPE_TOYOTA_VIOS',
       'BRAND_TYPE_TOYOTA_WISH', 'BRAND_TYPE_TOYOTA_YARIS']


    df = pd.DataFrame(columns = column_names)
    try:
        print(request.POST)
        brand = str(request.POST['brand']).replace(" ",'').replace("-",'').upper()
        type = str(request.POST['type']).replace(" ",'').replace("-",'').upper()
        BRAND_TYPE = brand + '_' + type
        mixName = 'BRAND_TYPE_' + BRAND_TYPE
        year = int(str(request.POST['YM_MFG']).split('.')[0])
        newPrice = 0
        try:
            newPrice = NewPrice[(NewPrice['BRAND'] == brand) & (NewPrice['TYPE'] == type) & (NewPrice['YEAR'] == year)]['AveragePrice'].values[0]
        except Exception as e:
            a = NewPrice.groupby(['BRAND', 'TYPE'])['AveragePrice'].mean()
            tmp = pd.merge(NewPrice, a, on=['BRAND', 'TYPE'], how='left')
            newPrice = tmp[(tmp['BRAND'] == brand) & (NewPrice['TYPE'] == type)]['AveragePrice_y'].values[0]

        df = df.append({
            'EXHAUST' : int(request.POST['EXHAUST']),
            'NUM_TRAVEL' : float(request.POST['NUM_TRAVEL']),
            'OLD' : ymMfgFunc(str(request.POST['YM_MFG']), str(request.POST['BSELL_DATE_NO'])),
            'CAREVU': carEvuFunc(str(request.POST['CAREVU'])),
            'INSIDEVU': insideEvuFunc(str(request.POST['INSIDEVU'])),
            'SOU': sellerType(request.POST['SOU']),
            'SalesMonth': int(str(int(request.POST['BSELL_DATE_NO']))[4:6]),
            'COLOR': colorFunx(request.POST['COLOR']),
            'AveragePrice': newPrice,
            mixName : 1},ignore_index=True)
        df = df.fillna(0)
    except Exception as e:
        print(e)

    try:
        result = getPredictions(df)
    except Exception as e:
        print(e)

    return render(request, 'result.html', {'result':result, 'origin': request.POST})

